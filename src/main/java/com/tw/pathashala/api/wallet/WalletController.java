package com.tw.pathashala.api.wallet;

import com.tw.pathashala.api.transaction.Transaction;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/wallets")
@CrossOrigin
public class WalletController {
    private static Logger logger = LogManager.getLogger(WalletController.class);

    private final WalletService walletService;

    public WalletController(@Autowired WalletService walletService) {
        this.walletService = walletService;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Wallet create(@Valid @RequestBody Wallet wallet) {
        logger.info(() -> "Creating api with amount: " + wallet.getBalance());
        return walletService.create(wallet);
    }

    @GetMapping("/{id}")
    Wallet fetch(@PathVariable Long id) {
        return walletService.fetch(id);
    }

    @PostMapping("/{walletId}/transactions")
    @ResponseStatus(HttpStatus.CREATED)
    Transaction createTransaction(@RequestBody Transaction transaction, @PathVariable long walletId) {
        return walletService.createTransaction(transaction, walletId);
    }

    @GetMapping(value = "/{walletId}/transactions")
    @ResponseStatus(HttpStatus.OK)
    List<Transaction> listTransactions(@PathVariable() long walletId,
                                       @ApiParam( value = "Enter number of record you wanted to display")
                                       @RequestParam(value = "limit", required = false, defaultValue = "0") long limit,
                                       @ApiParam(value = "Enter date in given ISO format YYYY-MM-DD")
                                       @DateTimeFormat(pattern = "yyyy-MM-dd")
                                       @RequestParam(value = "fromDate", required = false) Date fromDate,
                                       @ApiParam(value = "Enter date in given ISO format YYYY-MM-DD")
                                       @DateTimeFormat(pattern = "yyyy-MM-dd")
                                       @RequestParam(value = "toDate", required = false) Date toDate)
            throws Exception,Throwable {


        if (limit != 0) {
            return walletService.showRecentTransactionsForMobileScreen(walletId, limit);
        }
        if (fromDate != null && toDate != null) {
            return walletService.filterByDate(walletId, fromDate, toDate);
        }
        return walletService.showAllTransactions(walletId);
    }

}


