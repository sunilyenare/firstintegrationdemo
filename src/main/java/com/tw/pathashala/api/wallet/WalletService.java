package com.tw.pathashala.api.wallet;

import com.tw.pathashala.api.transaction.Transaction;
import com.tw.pathashala.api.transaction.TransactionRepository;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@Service
public class WalletService {

    private WalletRepository walletRepository;
    private TransactionRepository transactionRepository;

    WalletService(WalletRepository walletRepository, TransactionRepository transactionRepository) {
        this.walletRepository = walletRepository;
        this.transactionRepository = transactionRepository;
    }

    public Wallet create(Wallet wallet) {
        return walletRepository.save(wallet);
    }

    Wallet fetch(Long id) {
        return walletRepository.findById(id).orElseThrow(WalletNotFoundException::new);
    }

    Transaction createTransaction(Transaction transaction, long walletId) {
        Wallet savedWallet = fetch(walletId);
        savedWallet.processTransaction(transaction);
        Wallet updatedWallet = create(savedWallet);
        List<Transaction> transactions = updatedWallet.getTransactions();
        return transactions.get(transactions.size() - 1);
    }

    List<Transaction> showAllTransactions(Long walletId) throws TransactionNotFoundException, InvalidWalletException {
        Optional<Wallet> savedWallet = walletRepository.findById(walletId);
        if (savedWallet.isPresent()) {
            List<Transaction> transactions = transactionRepository.findByWalletId(walletId);
            return transactions;
        }
        throw new InvalidWalletException("wallet not present", "");
    }


    List<Transaction> showRecentTransactionsForMobileScreen(long walletId, long limit) throws InvalidWalletException, TransactionNotFoundException, LimitInvalidException {
        if (limit > 0) {
            Optional wallet_Id = walletRepository.findById(walletId);
            if (wallet_Id.isPresent()) {
                List<Transaction> recentTransactions = transactionRepository.findRecentTransaction(walletId, limit);
                return recentTransactions;
            }
            throw new InvalidWalletException("wallet", "");
        }
        throw new LimitInvalidException("limit ", "");
    }


    public List<Transaction> filterByDate(long walletId, Date fromDate, Date toDate) throws Exception {
        List<Transaction> allTransactions = fetch(walletId).getTransactions();
        List<Transaction> newTransactions = new ArrayList<>();


        if (fromDate.after(toDate)) {
            throw new DateFormatException("this date is not valid "+fromDate,"");
        }
        if (toDate.before(fromDate)) {
            throw new DateFormatException("this date is not valid "+toDate,"");
        }

        if (fromDate.after(new Date())) {
            throw new DateFormatException("Future date is not valid "+fromDate,"");
        }


        for (Transaction transaction : allTransactions) {
            if (transaction.getDate().after(fromDate) && transaction.getDate().before(toDate)) {
                newTransactions.add(transaction);
            }
        }
        return newTransactions;

    }



}
