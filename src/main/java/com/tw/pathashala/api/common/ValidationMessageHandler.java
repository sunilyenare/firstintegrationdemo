package com.tw.pathashala.api.common;

import com.tw.pathashala.api.error.ErrorResponse;
import com.tw.pathashala.api.error.UserNameAlreadyPresentException;
import com.tw.pathashala.api.wallet.DateFormatException;
import com.tw.pathashala.api.wallet.InvalidWalletException;
import com.tw.pathashala.api.wallet.LimitInvalidException;
import com.tw.pathashala.api.wallet.TransactionNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ValidationMessageHandler {
    static final String VALIDATION_FAILURE_MESSAGE = "Validation failure";
    static final String Wallet_FAILURE_MESSAGE = "wallet failure";
    static final String Transaction_FAILURE_MESSAGE = "transaction failure";
    static final String DATE_FAILURE_MESSAGE = "Date failure";
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse handleValidationErrors(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ErrorResponse(VALIDATION_FAILURE_MESSAGE, errors);
    }
//
//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//        Map<String, String> errors = new HashMap<>();
//        ex.getBindingResult().getAllErrors().forEach((error) -> {
//            String fieldName = ((FieldError) error).getField();
//            String errorMessage = error.getDefaultMessage();
//            errors.put(fieldName, errorMessage);
//        });
//        ErrorResponse errorResponse = new ErrorResponse(VALIDATION_FAILURE_MESSAGE, errors);
//        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
//    }

    @ExceptionHandler(LimitInvalidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse LimitInvalidException(LimitInvalidException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getKey(), ex.getValue() + " limit value must be positive");
        return new ErrorResponse(VALIDATION_FAILURE_MESSAGE, errors);
    }

    @ExceptionHandler(InvalidWalletException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse InvalidWalletException(InvalidWalletException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getKey(), ex.getValue() + "not present");
        return new ErrorResponse(Wallet_FAILURE_MESSAGE, errors);
    }

    @ExceptionHandler(TransactionNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse TransactionNotFoundException(TransactionNotFoundException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getKey(), ex.getValue() + "not available");
        return new ErrorResponse(Transaction_FAILURE_MESSAGE, errors);
    }

    @ExceptionHandler(UserNameAlreadyPresentException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    protected ErrorResponse handleUserPresentException(UserNameAlreadyPresentException ex) {
        System.out.println("In UserNAmeAlreadyPresentException handler");
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getKey(), ex.getValue());
        System.out.println();
        return new ErrorResponse("Sign Up failed", errors);
    }

    @ExceptionHandler(DateFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse DateFormatException(DateFormatException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getKey(), ex.getValue() + "not valid ");
        return new ErrorResponse(DATE_FAILURE_MESSAGE, errors);
    }

}
