package com.tw.pathashala.api.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tw.pathashala.api.wallet.Wallet;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "usertable")
public class User {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(hidden = true)
    private Long id;

    @JsonProperty
    @NotBlank(message = "username must not empty")
    @Column(nullable = false, unique = true)
    @Length(min = 3, max = 15, message = "The category name must be {min} to {max} characters in length.")
    private String userName;

    @JsonIgnore
    @JsonProperty
    @NotBlank(message = "password must not empty")
    @ApiModelProperty(position = 2, required = true)
    @Column(nullable = false)
    private String password;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Wallet wallet;

    public User(String userName, String password) {
        this.userName = userName;
        setPassword(password);
    }
    public User() {
    }

   public String getUserName() {
        return userName;
    }


    String getPassword() {
        return password;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }

    public Long walletId() {
        return wallet.getId();
    }

    @ApiModelProperty(position = 2, required = true)
    protected void setPassword(String password) {
        this.password = password;
    }
    @ApiModelProperty(position = 1, required = true)
    public void setUserName(String userName) {
        this.userName = userName;
    }
}
