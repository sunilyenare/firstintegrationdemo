package com.tw.pathashala.api.user;

import com.tw.pathashala.api.error.UserNameAlreadyPresentException;
import com.tw.pathashala.api.wallet.Wallet;
import com.tw.pathashala.api.wallet.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Autowired
    UserRepository userRepository;

    @Autowired
    WalletService walletService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return new org.springframework.security.core.userdetails.User(
                user.getUserName(),
                user.getPassword(),
                AuthorityUtils.createAuthorityList()
        );
    }

    public User findUserByUsername(String username) {
        return userRepository.findByUserName(username).orElseThrow(() -> new UsernameNotFoundException("User not found."));
    }


    public User create(User user) throws UserNameAlreadyPresentException {
        Optional<User> getUser = userRepository.findByUserName(user.getUserName());
        if ( getUser.isPresent()) {
            throw new UserNameAlreadyPresentException("username", user.getUserName()+" UserName is Already in use.");
        }
            user.setPassword(PASSWORD_ENCODER.encode(user.getPassword()));
            User saveduser = userRepository.save(user);
            walletService.create(new Wallet("primary", 0, saveduser));
            return saveduser;
    }
}
