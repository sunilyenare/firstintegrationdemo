package com.tw.pathashala.api.user;

import com.tw.pathashala.api.error.UserNameAlreadyPresentException;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping
    @ApiResponses(value = {@ApiResponse(code = 201, message = "user details"),
            @ApiResponse(code = 409, message = "username in use."),
            @ApiResponse(code = 400, message = "validation errors.")})
    public ResponseEntity create(@Valid @RequestBody User user) throws UserNameAlreadyPresentException {
        User savedUser = userService.create(user);
        return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
    }
}
