package com.tw.pathashala.api;

import com.tw.pathashala.api.user.User;
import com.tw.pathashala.api.user.UserRepository;
import com.tw.pathashala.api.user.UserService;
import com.tw.pathashala.api.wallet.Wallet;
import com.tw.pathashala.api.wallet.WalletService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
public class DataSeeder {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Bean
    CommandLineRunner initDatabase(UserRepository repository, UserService userService, WalletService walletService) {
        return args -> {
            if (repository.findByUserName("seed-user-1").isEmpty()) {
                User savedUser = userService.create(new User("seed-user-1",PASSWORD_ENCODER.encode("foobar")));
            }
            if (repository.findByUserName("seed-user-2").isEmpty()) {
                User savedUser = userService.create(new User("seed-user-2", PASSWORD_ENCODER.encode("foobar")));
            }
        };
    }
}
