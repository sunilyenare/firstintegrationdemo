package com.tw.pathashala.api.transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findByWalletId(Long walletId);

   @Query(value = "select * FROM transaction where wallet_id=:walletId order by date desc limit :limit" ,nativeQuery=true)
    List<Transaction> findRecentTransaction(Long walletId,long limit);
 }
