package com.tw.pathashala.api.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tw.pathashala.api.transaction.Transaction;
import com.tw.pathashala.api.transaction.TransactionType;
import com.tw.pathashala.api.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(WalletController.class)
@WithMockUser
class WalletControllerTest {

    private static final String BASE_PATH = "/wallets";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @MockBean
    UserRepository userRepository;

    @Test
    void createAWalletUsingWalletResource() throws Exception {
        when(walletService.create(any(Wallet.class))).thenReturn(wallet());
        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post(BASE_PATH + "/")
                .content("{\"name\":\"Walter White\",\"balance\":100}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(objectMapper.writeValueAsString(wallet())));

        verify(walletService).create(any(Wallet.class));
    }

    @Test
    void expectCreateWalletToFailWhenNameIsLongerThan12Characters() throws Exception {
        mockMvc.perform(post(BASE_PATH + "/")
                .content("{\"name\":\"ThisNameIsTooLongForWallet\",\"balance\":100}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(walletService, times(0)).create(any(Wallet.class));
    }

    @Test
    void shouldGetAWallet() throws Exception {
        when(walletService.fetch(1L)).thenReturn(wallet());
        mockMvc.perform(get(BASE_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value("100"));

    }

    @Test
    void shouldReturnNotFoundWhenWalletDoesNotExist() throws Exception {
        when(walletService.fetch(12L)).thenThrow(new WalletNotFoundException());
        mockMvc.perform(get(BASE_PATH + "/12")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void createATransactionOnAWallet() throws Exception {
        Transaction savedTransaction = new Transaction(TransactionType.CREDIT, 10);
        ObjectMapper objectMapper = new ObjectMapper();
        when(walletService.createTransaction(any(Transaction.class), eq(1L)))
                .thenReturn(savedTransaction);

        mockMvc.perform(post(BASE_PATH + "/1/transactions")
                .content("{\"type\":\"CREDIT\",\"amount\":10}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(objectMapper.writeValueAsString(savedTransaction)));

        verify(walletService).createTransaction(any(Transaction.class), eq(1L));
    }

    @Test
    void expectCrossOriginRequestsToBeSuccessful() throws Exception {
        this.mockMvc
                .perform(get("/wallets/1")
                        .header("Origin", "http://dumyurl.com"))
                .andExpect(status().isOk())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

//    @Test
//    void expectToShowAllTransactionsOfUser() throws Exception {
//        List<Transaction> transactions = new ArrayList<>();
//        Transaction transactionOne = new Transaction(TransactionType.CREDIT, 102);
//        Transaction transactionTwo = new Transaction(TransactionType.DEBIT, 40);
//        transactions.add(transactionOne);
//        transactions.add(transactionTwo);
//        ObjectMapper objectMapper = new ObjectMapper();
//
//        when(walletService.showAllTransactions(anyLong())).thenReturn(transactions);
//
//        mockMvc.perform(get(("/wallets/1/transactions")).param("limit", "0")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.[0].amount").value(102))
//                .andExpect(content().string(objectMapper.writeValueAsString(transactions)));
//
//        verify(walletService, times(1)).showAllTransactions(1L);
//    }

    @Test
    void exceptToShowRecentFiveTransactionsOfUser() throws Exception, TransactionNotFoundException, LimitInvalidException {
        List<Transaction> transactions = new ArrayList<>();
        Transaction transactionOne = new Transaction(TransactionType.CREDIT, 102);
        Transaction transactionTwo = new Transaction(TransactionType.DEBIT, 30);
        Transaction transactionThree = new Transaction(TransactionType.DEBIT, 35);
        Transaction transactionFour = new Transaction(TransactionType.DEBIT, 56);
        Transaction transactionFive = new Transaction(TransactionType.DEBIT, 47);
        transactions.add(transactionFive);
        transactions.add(transactionFour);
        transactions.add(transactionThree);
        transactions.add(transactionTwo);
        transactions.add(transactionOne);
        ObjectMapper objectMapper = new ObjectMapper();

        when(walletService.showRecentTransactionsForMobileScreen(anyLong(), anyLong())).thenReturn(Arrays.asList(transactionFive, transactionFour, transactionThree, transactionTwo, transactionOne));
        mockMvc.perform(get(("/wallets/1/transactions")).param("limit", "5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].amount").value(47))
                .andExpect(jsonPath("$[3].amount").value(30))
                .andExpect(content().string(objectMapper.writeValueAsString(transactions)));
    }


    @Test
    void getTransactionsOfFilterByDate() throws Exception {

        when(walletService.filterByDate(anyLong(),any(Date.class),any(Date.class))).thenReturn(new ArrayList<Transaction>());
        mockMvc.perform(get(("/wallets/1/transactions"))
                .param("fromDate", "2019-01-01")
                .param("toDate","2019-02-02")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void shouldListAllTransactions() throws Exception {
        when(walletService.filterByDate(anyLong(),any(Date.class),any(Date.class))).thenReturn(new ArrayList<Transaction>());
        mockMvc.perform(get(("/wallets/1/transactions"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void getTransactionsOfFilterByDateIfWalletIsInValidThenThrowWalletNotFoundException() throws Exception {
        when(walletService.filterByDate(anyLong(),any(Date.class),any(Date.class))).thenThrow(new WalletNotFoundException());
        mockMvc.perform(get(("/wallets/55/transactions"))
                .param("fromDate", "2019-01-01")
                .param("toDate","2019-01-02")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }
    private Wallet wallet() {
        Wallet wallet = new Wallet("Walter White", 100);
        wallet.setId(1L);
        return wallet;
    }
}
