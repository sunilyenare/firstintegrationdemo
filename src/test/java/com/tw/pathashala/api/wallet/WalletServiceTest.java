package com.tw.pathashala.api.wallet;

import com.tw.pathashala.api.transaction.Transaction;
import com.tw.pathashala.api.transaction.TransactionRepository;
import com.tw.pathashala.api.transaction.TransactionType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WalletServiceTest {

    @Autowired
    WalletRepository walletRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    WalletService walletService;

    @AfterEach
    void tearDown() {
        System.out.println("teardown running...");
        transactionRepository.deleteAll();
        walletRepository.deleteAll();
    }

    @Test
    void createAWallet() {
        WalletService walletService = new WalletService(walletRepository, transactionRepository);

        Wallet wallet = walletService.create(new Wallet("Walter White", 100));

        Optional<Wallet> walletFromStore = walletRepository.findById(wallet.getId());
        assertEquals("Walter White", wallet.getName());
        assertEquals("Walter White", walletFromStore.get().getName());
    }

    @Test
    void fetchAWallet() {
        Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
        WalletService walletService = new WalletService(walletRepository, transactionRepository);

        Wallet wallet = walletService.fetch(savedWallet.getId());

        assertEquals("Walter White", wallet.getName());
    }

    @Test
    void failsToFetchAWalletWithInvalidId() {
        long invalidId = 999L;
        WalletService walletService = new WalletService(walletRepository, transactionRepository);

        assertThrows(WalletNotFoundException.class, () -> walletService.fetch(invalidId));
    }

    @Test
    void credit50IntoTheWallet() {
        Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
        WalletService walletService = new WalletService(walletRepository, transactionRepository);
        Transaction newTransaction = new Transaction(TransactionType.CREDIT, 50);

        Transaction savedTransaction = walletService.createTransaction(newTransaction, savedWallet.getId());

        savedWallet = walletService.fetch(savedWallet.getId());
        System.out.println(savedWallet.getTransactions().size());
        assertEquals(1, savedWallet.getTransactions().size());
        assertEquals(150, savedWallet.getBalance());
        assertNotEquals(0, savedTransaction.getId());
    }

    @Test
    void credit50FromTheWallet() {
        System.out.println("transactionRepository.findAll().size()" + transactionRepository.findAll().size());
        Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
        WalletService walletService = new WalletService(walletRepository, transactionRepository);
        Transaction newTransaction = new Transaction(TransactionType.DEBIT, 50);

        Transaction savedTransaction = walletService.createTransaction(newTransaction, savedWallet.getId());

        savedWallet = walletService.fetch(savedWallet.getId());
        assertEquals(1, savedWallet.getTransactions().size());
        assertEquals(50, savedWallet.getBalance());
        assertEquals(1, savedTransaction.getId());
    }

    @Test
    void showAllTransactionsOfParticularUser() throws InvalidWalletException, TransactionNotFoundException {
        Wallet savedWalletOfUserOne = walletRepository.save(new Wallet("Dipali", 500));
        Wallet savedWalletOfUserTwo = walletRepository.save(new Wallet("Sneha", 600));
        WalletService walletService = new WalletService(walletRepository, transactionRepository);
        Transaction newTransaction = new Transaction(TransactionType.DEBIT, 20);
        Transaction newTransactionOne = new Transaction(TransactionType.DEBIT, 300);

        walletService.createTransaction(newTransaction, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionOne, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionOne, savedWalletOfUserTwo.getId());

        assertEquals(2, walletService.showAllTransactions(savedWalletOfUserOne.getId()).size());
        assertEquals(1, walletService.showAllTransactions(savedWalletOfUserTwo.getId()).size());
    }

    @Test
    void showFiveRecentTransactions() throws InvalidWalletException, TransactionNotFoundException, LimitInvalidException {
        Wallet savedWalletOfUserOne = walletRepository.save(new Wallet("Dipali", 500));
        WalletService walletService = new WalletService(walletRepository, transactionRepository);
        Transaction newTransaction = new Transaction(TransactionType.DEBIT, 20);

        Transaction newTransactionOne = new Transaction(TransactionType.DEBIT, 30);
        Transaction newTransactionTwo = new Transaction(TransactionType.DEBIT, 30);
        Transaction newTransactionThree = new Transaction(TransactionType.DEBIT, 30);
        Transaction newTransactionFour = new Transaction(TransactionType.DEBIT, 30);
        Transaction newTransactionFive = new Transaction(TransactionType.DEBIT, 30);

        walletService.createTransaction(newTransaction, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionOne, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionTwo, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionThree, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionFour, savedWalletOfUserOne.getId());
        walletService.createTransaction(newTransactionFive, savedWalletOfUserOne.getId());
        assertEquals(5, walletService.showRecentTransactionsForMobileScreen(savedWalletOfUserOne.getId(), 5).size());

    }

    @Test
    void WhenNegativeLimit() throws InvalidWalletException, TransactionNotFoundException, LimitInvalidException {
        Wallet savedWalletOfUserOne = new Wallet("swapnil", 100);
        walletRepository.save(savedWalletOfUserOne);
        assertThrows(LimitInvalidException.class, () -> walletService.showRecentTransactionsForMobileScreen(savedWalletOfUserOne.getId(), -4));
    }


    @Nested
    class FilterByDate {

        @Test
        void givenTransactions_whenWeApplyFilterBy3Months_ThenItShouldGiveLast3MothsTransactions() throws Exception {
            Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
            WalletService walletService = new WalletService(walletRepository, transactionRepository);//Transaction transaction=new Transaction(savedWallet,TransactionType.CREDIT,50,new GregorianCalendar(2019,8,1).getTime(),"snacks");
            Transaction oneTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 50, new GregorianCalendar(2019, 8, 1).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());
            Transaction twoTransaction = new Transaction(savedWallet, TransactionType.DEBIT, 50, new GregorianCalendar(2019, 9, 2).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());
            Transaction threeTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 500, new GregorianCalendar(2019, 1, 3).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());

            Transaction savedOneTransaction = walletService.createTransaction(oneTransaction, savedWallet.getId());
            Transaction savedTwoTransaction = walletService.createTransaction(twoTransaction, savedWallet.getId());
            Transaction savedThreeTransaction = walletService.createTransaction(threeTransaction, savedWallet.getId());

            List<Transaction> filterByDateTransactions = walletService.filterByDate(savedWallet.getId(), new GregorianCalendar(2019, 7, 21).getTime(), new GregorianCalendar(2019, 10, 21).getTime());

            assertEquals(2, filterByDateTransactions.size());
            assertEquals(new GregorianCalendar(2019, 8, 1).getTime(), filterByDateTransactions.get(0).getDate());

        }
        @Test
        void givenTransactions_whenWeApplyFilterBy3Months_ThenItShouldNotGiveTransactionsWhichNotInThatRange() throws Exception {
            Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
            WalletService walletService = new WalletService(walletRepository, transactionRepository);//Transaction transaction=new Transaction(savedWallet,TransactionType.CREDIT,50,new GregorianCalendar(2019,8,1).getTime(),"snacks");
            Transaction oneTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 50, new GregorianCalendar(2019, 8, 1).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());
            Transaction twoTransaction = new Transaction(savedWallet, TransactionType.DEBIT, 50, new GregorianCalendar(2019, 9, 2).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());
            Transaction threeTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 500, new GregorianCalendar(2019, 6, 3).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());

            Transaction savedOneTransaction = walletService.createTransaction(oneTransaction, savedWallet.getId());
            Transaction savedTwoTransaction = walletService.createTransaction(twoTransaction, savedWallet.getId());
            Transaction savedThreeTransaction = walletService.createTransaction(threeTransaction, savedWallet.getId());


            List<Transaction> filterByDateTransactions = walletService.filterByDate(savedWallet.getId(), new GregorianCalendar(2019, 7, 21).getTime(), new GregorianCalendar(2019, 10, 21).getTime());

            assertEquals(2, filterByDateTransactions.size());
            assertNotEquals(true, filterByDateTransactions.contains(threeTransaction));
        }


        @Test
        void givenTransactions_whenWeApplyFilterBy6Months_ThenItShouldGiveLast6MothsTransactions() throws Exception {
            Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
            WalletService walletService = new WalletService(walletRepository, transactionRepository);//Transaction transaction=new Transaction(savedWallet,TransactionType.CREDIT,50,new GregorianCalendar(2019,8,1).getTime(),"snacks");

            Transaction oneTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 50, new GregorianCalendar(2019, 2, 1).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());
            Transaction twoTransaction = new Transaction(savedWallet, TransactionType.DEBIT, 50, new GregorianCalendar(2019, 9, 2).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());
            Transaction threeTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 500, new GregorianCalendar(2019, 6, 3).getTime(), "testOne");// newTransaction.setDate(new GregorianCalendar(2019,8,1).getTime());

            Transaction savedOneTransaction = walletService.createTransaction(oneTransaction, savedWallet.getId());
            Transaction savedTwoTransaction = walletService.createTransaction(twoTransaction, savedWallet.getId());
            Transaction savedThreeTransaction = walletService.createTransaction(threeTransaction, savedWallet.getId());

            List<Transaction> filterByDateTransactions = walletService.filterByDate(savedWallet.getId(), new GregorianCalendar(2019, 4, 21).getTime(), new GregorianCalendar(2019, 10, 21).getTime());

            assertEquals(2, filterByDateTransactions.size());
            assertEquals(false, filterByDateTransactions.contains(new GregorianCalendar(2019, 2, 1).getTime()));

        }

        @Test
        void givenTransactions_whenWeApplyFilterByDate_ThenItShouldNotTakeFromDateMoreThanCurrentDate() throws Exception {
            Wallet savedWallet = walletRepository.save(new Wallet("Walter White", 100));
            WalletService walletService = new WalletService(walletRepository, transactionRepository);//Transaction transaction=new Transaction(savedWallet,TransactionType.CREDIT,50,new GregorianCalendar(2019,8,1).getTime(),"snacks");

            Transaction oneTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 50, new GregorianCalendar(2019, 2, 1).getTime(), "testOne");
            Transaction twoTransaction = new Transaction(savedWallet, TransactionType.DEBIT, 50, new GregorianCalendar(2019, 9, 2).getTime(), "testOne");
            Transaction threeTransaction = new Transaction(savedWallet, TransactionType.CREDIT, 500, new GregorianCalendar(2019, 6, 3).getTime(), "testOne");

            Transaction savedOneTransaction = walletService.createTransaction(oneTransaction, savedWallet.getId());
            Transaction savedTwoTransaction = walletService.createTransaction(twoTransaction, savedWallet.getId());
            Transaction savedThreeTransaction = walletService.createTransaction(threeTransaction, savedWallet.getId());

            List<Transaction> filterByDateTransactions = walletService.filterByDate(savedWallet.getId(), new GregorianCalendar(2019, 4, 21).getTime(), new GregorianCalendar(2019, 10, 21).getTime());

            assertEquals(2, filterByDateTransactions.size());
            assertEquals(false, filterByDateTransactions.contains(new GregorianCalendar(2019, 2, 1).getTime()));

        }

        @Test
        void givenInvalidWalletId_whenTransactionsFilterByDate_ThenItShouldBeThrowWalletNotFound() {
            int invalidWalletId = 55;
            WalletService walletService = new WalletService(walletRepository, transactionRepository);

            Assertions.assertThrows(WalletNotFoundException.class, () -> {
                walletService.filterByDate(invalidWalletId, new GregorianCalendar(2019, 4, 21).getTime(), new GregorianCalendar(2019, 10, 21).getTime());
            });

        }

//        @Test
//        public void testDayIsInvalid() {
//            assertFalse(walletService.isThisDateValid(new GregorianCalendar(2019, 2, 1).getTime(),new GregorianCalendar(2019, 4, 1)));
//        }
    }


}
