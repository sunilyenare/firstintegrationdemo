package com.tw.pathashala.api.user;

import com.tw.pathashala.api.DataSeeder;
import com.tw.pathashala.api.error.UserNameAlreadyPresentException;
import com.tw.pathashala.api.transaction.TransactionRepository;
import com.tw.pathashala.api.wallet.WalletService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
 class UserControllerTest {


    private static final String BASE_PATH = "/users";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    WalletService walletService;

    @MockBean
    TransactionRepository transactionRepository;


    @Test
    void givenAnAlreadyExistingUser_WhenSigningUp_ThenReturnBadRequest() throws Exception {
        when(userService.create(any(User.class))).thenThrow(UserNameAlreadyPresentException.class);

        mockMvc.perform(post("/users")
                .content("{\"userName\":\"xyz\",\"password\":\"abc\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());

       // verify(userService).create(any(User.class));
    }
}

