package com.tw.pathashala.api.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserTest {

    @Test
    void expectedUserWithNameAndPasswordAfterSerialization() throws JsonProcessingException {
        User userOne = new User("shyam", "shyam123");
        ObjectMapper objectMapper = new ObjectMapper();


        String userString = objectMapper.writeValueAsString(userOne);

        assertTrue(userString.contains("\"userName\":\"shyam\""));

    }

    @Test
    void expectedUserWithUserNameAndPasswordAfterDeserialization() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String userSting = "{\"userName\":\"shyam\", \"password\":\"shyam123\"}";
        User user = objectMapper.readValue(userSting, User.class);

        assertEquals("shyam", user.getUserName());
    }
}
