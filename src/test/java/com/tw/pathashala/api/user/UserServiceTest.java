package com.tw.pathashala.api.user;

import com.tw.pathashala.api.error.UserNameAlreadyPresentException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class UserServiceTest {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Nested
    class LoadUserByUsername {
        @Test
        void shouldLoadWhenUserExists() {
            User user = new User("John", "FooBar");
            userRepository.save(user);

            UserDetails userDetails = userService.loadUserByUsername("John");
            assertEquals("John", userDetails.getUsername());
        }

        @Test
        void shouldNotLoadWhenUserDoesNotExist() {
            assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername("Jane"));
        }

        @Test
        void givenUser_WhenCreate_MustAddToDatabase() throws Exception {
            User user = userService.create(new User("abcd", "abcd"));
            Optional<User> storedUser = userRepository.findByUserName("abcd");
            assertEquals(storedUser.get().getUserName(), user.getUserName());
        }

        @Test
        void givenUserWhichIsAlreadyPresent_whenCreate_ThenItThrowsException() throws Exception {
            User user = new User("abcdw", "abcdqw");
            userService.create(user);
            assertThrows(UserNameAlreadyPresentException.class, () -> userService.create(user));

        }

        @Test
        void givenUser_WhenCreate_MustGetMeAWalletId() throws Exception {
            User newuser=new User("abcdq", "abcdq");
            User user = userService.create(newuser);
            Optional<User> storedUser = userRepository.findByUserName("abcdq");
            assertEquals(storedUser.get().getUserName(), user.getUserName());
        }
    }
}